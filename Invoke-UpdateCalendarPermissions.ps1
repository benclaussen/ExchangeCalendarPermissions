﻿param
(
	[Parameter(Position = 0)]
	[string]$ConfigurationPath = ".\Configuration.xml",
	
	[Parameter(Position = 1)]
	[string]$LogPath = ".\Logs\UpdatePermissions_$(Get-Date -Format "yyyyMMdd-HHmmss").log",
	
	[Parameter(Position = 2)]
	[switch]$CommitChanges
)

[xml]$config = Get-Content $ConfigurationPath -ErrorAction Stop

function Write-Log {
	#region Parameters
	
	[cmdletbinding()]
	Param (
		[Parameter(ValueFromPipeline = $true, Mandatory = $true)]
		[ValidateNotNullOrEmpty()]
		[string]$Message,
		
		[Parameter()]
		[ValidateSet('Error', 'Warn', 'Info', 'Verbose', 'Debug')]
		[string]$Level = 'Info',
		
		[Parameter()]
		[Switch]$NoConsoleOut,
		
		[Parameter()]
		[System.ConsoleColor]$ConsoleForeground = $((Get-Host).UI.RawUI.ForegroundColor),
		
		[Parameter()]
		[ValidateRange(1, 30)]
		[Int16]$Indent = 0,
		
		[Parameter()]
		[IO.FileInfo]$Path = "$script:logPath",
		
		[Parameter()]
		[Switch]$Clobber,
		
		[Parameter()]
		[String]$EventLogName,
		
		[Parameter()]
		[String]$EventSource,
		
		[Parameter()]
		[Int32]$EventID = 1
		
	)
	
	#endregion
	
	Begin {}
	
	Process {
		if (($Level -eq 'Debug') -and ($DebugPreference -eq 'SilentlyContinue')) {
			return
		} elseif (($Level -eq 'Verbose') -and ($VerbosePreference -eq 'SilentlyContinue')) {
			return
		}
		
		try {					
			if (-not (Test-Path -Path $Path -PathType Leaf)) {
				$null = New-Item $Path -Force -ItemType File
			}
			
			[string]$datetime = (Get-Date -Format 'yyyy-MM-dd HH:mm:ss.fff')
			
			$msg = '{0},{1},{2}' -f $datetime, $Level.ToUpper(), $Message
			
			if ($NoConsoleOut -eq $false) {
				switch ($Level) {
					'Error' {
						Write-Host "ERROR:   $Message" -ForegroundColor 'Red'
					}
					'Warn' {
						Write-Warning $Message
					}
					'Info' {
						Write-Host $Message -ForegroundColor $ConsoleForeground
					}
					'Verbose' {
						Write-Verbose $Message
					}
					'Debug' {
						Write-Debug $Message
					}
				}
			}
			
			if ($Clobber) {
				$msg | Out-File -FilePath $Path -Force -Encoding Utf8
			} else {
				$msg | Out-File -FilePath $Path -Append -Encoding Utf8
			}
			
			if ($EventLogName) {
				
				if (-not $EventSource) {
					$EventSource = ([IO.FileInfo]$MyInvocation.ScriptName).Name
				}
				
				if (-not [Diagnostics.EventLog]::SourceExists($EventSource)) {
					[Diagnostics.EventLog]::CreateEventSource($EventSource, $EventLogName)
				}
				
				$log = New-Object System.Diagnostics.EventLog
				$log.set_log($EventLogName)
				$log.set_source($EventSource)
				
				switch ($Level) {
					'Error' {
						$log.WriteEntry($Message, 'Error', $EventID)
					}
					'Warn'  {
						$log.WriteEntry($Message, 'Warning', $EventID)
					}
					'Info'  {
						$log.WriteEntry($Message, 'Information', $EventID)
					}
					default {
						$log.WriteEntry($Message, 'Information', $EventID)
					}
				}
			}
			
		} catch {
			Write-Error -Category 'OpenError' -CategoryActivity 'Writing to log file' -CategoryReason "$_"
		}
	}
	
	End {}
}

function Get-BooleanValue {
	[CmdletBinding()]
	[OutputType([boolean])]
	param
	(
		[Parameter(Mandatory = $true, Position = 0, ValueFromPipeline = $true)]
		[string]$Value
	)
	
	BEGIN {
	}
	PROCESS {
		if ($Value.length -gt 1) {
			return $Value -ieq 'true'
		}
		
		return [bool][int]$Value
	}
	END {
	}
}

function Build-NewPermissions {
	[CmdletBinding()]
	[OutputType([hashtable])]
	param
	(
		[Parameter(Mandatory = $true,
				   Position = 0)]
		[System.Xml.XmlElement]$ResourceElement,
		
		[Parameter(Mandatory = $true,
				   Position = 1)]
		[System.Xml.XmlElement]$MasterPermissions
	)
	
	function Update-ArrayList {
		param
		(
			[Parameter(Mandatory = $true,
					   Position = 0)]
			$ArrayList,
			
			[Parameter(Mandatory = $true,
					   Position = 1)]
			[string]$Identity,
			
			[Parameter(Mandatory = $true,
					   Position = 2)]
			[string]$AccessRights,
			
			[Parameter(Position = 3)]
			[string]$Type = 'Internal'
		)
		
		if ($update = $ArrayList | Where-Object { $_.Identity -eq $Identity }) {
			# Update
			Write-Log -Level Verbose -Message "Updating existing entry for $Identity from $($update.AccessRights) to $AccessRights"
			$update.AccessRights = $AccessRights
			$update.Type = $Type
		} else {
			# Add
			#$ace = "" | Select-Object "Identity", "AccessRights", "Type"
			$ace = [pscustomobject]@{
				"Identity" = $null
				"AccessRights" = $null
				"Type" = $null
			}
			$ace | Add-Member -MemberType ScriptMethod -Name "ToString" -Value { '{0}:{1}:{2}' -f $this.Identity, $this.AccessRights, $this.Type } -Force
			Write-Log -Level Verbose -Message "Adding new entry for $Identity : $AccessRights"
			$ace.Identity = $Identity
			$ace.AccessRights = $AccessRights
			$ace.Type = $Type
			$null = $ArrayList.Add($ace)
		}
	}
		
	[System.Collections.ArrayList]$permissionsArray = New-Object System.Collections.Arraylist
	
	Write-Log -Level Verbose -Message "Evaluating resource permissions"
	foreach ($permission in $ResourceElement.Permissions.Permission) {
		if (($permission.Type -eq "Group") -and ((Get-BooleanValue -Value $permission.Recurse) -eq $true)) {
			Write-Log -Level Verbose "Obtaining distribution group members: $($permission.Identity)"
			$dlmembers = Get-DLMembers -Identity $permission.Identity
			foreach ($user in $dlmembers) {
				Write-Log -Level Verbose -Message "Processing $($permission.Identity) : $($permission.AccessRights)"
				Update-ArrayList -ArrayList $permissionsArray -Identity $user.User -AccessRights $permission.AccessRights -Type $user.Type
			}
		} else {
			Write-Log -Level Verbose -Message "Processing $($permission.Identity) : $($permission.AccessRights)"
			Update-ArrayList -ArrayList $permissionsArray -Identity $permission.Identity -AccessRights $permission.AccessRights
		}
	}
	
	if ((Get-BooleanValue -Value $ResourceElement.IgnoreMasterPermissions) -eq $false) {
		Write-Log -Level Verbose -Message "Evaluating master permissions"
		foreach ($permission in $MasterPermissions.Permission) {
			Write-Log -Level Verbose -Message "Processing $($permission.Identity) : $($permission.AccessRights)"
			Update-ArrayList -ArrayList $permissionsArray -Identity $permission.Identity -AccessRights $permission.AccessRights
		}
	} else {
		Write-Log -Level Warn -Message "Skipping master permissions because it is disabled for $($ResourceElement.Identity)"
	}
	
	return $permissionsArray
}

function Get-DLMembers {
	[CmdletBinding()]
	param
	(
		[Parameter(Mandatory = $true,
				   Position = 0)]
		[string]$Identity
	)
	
	$dl = Get-DistributionGroupMember -Identity $Identity |
	Where-Object {
		$_.RecipientType -eq "UserMailbox"
	} |
	Select-Object @{
		Name = "User"; Expression = {
			$_.PrimarySmtpAddress.ToString()
		}
	}, @{
		Name = "Type"; Expression = {
			"Internal"
		}
	} |
	Sort-Object "type", "user"
	
	return $dl
}

function Get-CurrentPermissions {
	[CmdletBinding()]
	param
	(
		[Parameter(Mandatory = $true,
				   Position = 0)]
		[String]$Identity
	)
	
	Get-MailboxFolderPermission -Identity $Identity |
	Select-Object @{
		Name = "Identity";
		Expression = {
			if ($_.User.UserType.ToString() -in @("Default", "Anonymous")) {
				$_.User.UserType.ToString()
			} else {
				$_.User.ADRecipient.PrimarySmtpAddress.ToString()
			}
		}
	}, @{
		Name = "Type";
		Expression = {
			$_.User.UserType.ToString()
		}
	}, @{
		Name = "DisplayName";
		Expression = {
			$_.User.DisplayName
		}
	}, @{
		Name = "AccessRights";
		Expression = {
			$_.AccessRights
		}
	} |
	Sort-Object type, identity
}

function Remove-UnknownPermissions {
	[CmdletBinding()]
	param
	(
		[Parameter(Mandatory = $true,
				   Position = 0)]
		[string]$Identity
	)
	
	# Deleted accounts will appear as unknown types. This is necessary to delete them.
	
	$unknowns = Get-MailboxFolderPermission -Identity $Identity | Where-Object { $_.User.UserType -eq "Unknown" }
	
	if (-not $unknowns) {
		Write-Log -Level Info -Message "No unknown permissions to remove"
		return
	}
	
	foreach ($unknownPermission in $unknowns) {
		$msg = "Removing unknown type permission ($($unknownPermission.User.DisplayName)) from $Identity"
		if ($CommitChanges) {
			Write-Log -Level Warn -Message $msg
			$unknownPermission | Remove-MailboxFolderPermission -Confirm:$false
		} else {
			Write-Log -Level Warn -Message "NOCOMMIT: $msg"
		}
	}
}

function Join-FolderPath {
	[CmdletBinding()]
	[OutputType([string])]
	param
	(
		[Parameter(Mandatory = $true,
				   Position = 0)]
		[System.Xml.Xmlelement]$ResourceElement
	)
	
	# Combines the email address and path to the folder for permissions
	
	return "{0}:{1}" -f $ResourceElement.Identity, $ResourceElement.Path
}

function Add-Permission {
	[CmdletBinding()]
	param
	(
		[Parameter(Mandatory = $true, Position = 0)]
		[string]$Identity,
		
		[Parameter(Mandatory = $true)]
		$GroupedPermission
	)
	
	# Add a new permission
	
	$msg = "Adding permission for user $($GroupedPermission.Group.Identity) : $($GroupedPermission.Group.AccessRights)"
	
	if (-not $CommitChanges) {
		# We are not committing changes...pretend we did something
		Write-Log -Level Info -Message "NO COMMIT: $msg"
		return
	}
	
	Write-Log -Level Info -Message $msg
	$null = Add-MailboxFolderPermission -Identity $identity -User $GroupedPermission.Group.Identity -AccessRights $GroupedPermission.Group.AccessRights -confirm:$false -ErrorAction Stop
}

function Remove-Permission {
	[CmdletBinding()]
	param
	(
		[Parameter(Mandatory = $true, Position = 0)]
		[string]$Identity,
		
		[Parameter(Mandatory = $true)]
		$GroupedPermission
	)
	
	# This permission is no longer needed... so remove it.
	
	$msg = "Removing permission for user $($GroupedPermission.Group.Identity)"
	
	if (-not $CommitChanges) {
		# We are not committing changes...pretend we did something
		Write-Log -Level Info -Message "NO COMMIT: $msg"
		return
	}
	
	Write-Log -Level Info -Message $msg
	$null = Remove-MailboxFolderPermission -Identity $identity -User $GroupedPermission.Group.Identity -confirm:$false -ErrorAction Stop
}

function Remove-DisabledAccountPermission {
	[CmdletBinding()]
	param
	(
		[Parameter(Mandatory = $true)]
		$UserMailbox,
		
		[Parameter(Mandatory = $true)]
		$ResourceIdentity
	)
	
	# Because of Exchange's annoying bug with disabled AD accounts, 
	# we have to remove the permission by Microsoft.Exchange.Data.Directory.Management.Mailbox object
	
	$msg = "Removing permission for disabled user $($UserMailbox.PrimarySmtpAddress.ToString())"
	
	if (-not $CommitChanges) {
		# We are not committing changes...pretend we did something
		Write-Log -Level Info -Message "NO COMMIT: $msg"
		return
	}
	
	try {
		Write-Log -Level Info -Message "$msg"
		$null = Get-MailboxFolderPermission -Identity $ResourceIdentity -User $UserMailbox | Remove-MailboxFolderPermission -Confirm:$false -ErrorAction Stop
	} catch {
		Write-Log -Level Error -Message "Failed to remove disabled account from permissions: $($_.Exception.Message)"
	}
	
}

function Set-Permission {
	[CmdletBinding()]
	param
	(
		[Parameter(Mandatory = $true, Position = 0)]
		[string]$Identity,
		
		[Parameter(Mandatory = $true)]
		$GroupedPermission
	)
	
	# A permission already exists so we are going to update the access rights
	
	$newAccessRights = ($GroupedPermission.Group | Where-Object { $_.SideIndicator -eq "=>"}).AccessRights
	$oldAccessRights = ($GroupedPermission.Group | Where-Object { $_.SideIndicator -eq "<="}).AccessRights
	
	$msg = "Updating permissions for user $($GroupedPermission.Group.Identity[0]) : $newAccessRights (from $oldAccessRights)"
	
	if (-not $CommitChanges) {
		# We are not committing changes...pretend we did something
		Write-Log -Level Info -Message "NO COMMIT: $msg"
		return
	}
	
	Write-Log -Level Info -Message $msg
	$null = Set-MailboxFolderPermission -Identity $Identity -User $($GroupedPermission.Group.Identity[0]) -AccessRights $newAccessRights -ErrorAction Stop
}

function Update-FolderPermissions {
	[CmdletBinding()]
	param
	(
		[Parameter(Mandatory = $true, Position = 0)]
		[string]$Identity,

		[Parameter(Mandatory = $true)]
		$GroupedPermission
	)
	
	if ($GroupedPermission.Count -eq 1) {
		Write-Log -Level Verbose -Message "One permission found in group"
		# Determine if need to add or remove permission
		switch ($GroupedPermission.Group.SideIndicator) {
			"=>" {
				# Add a permission
				try {
					Add-Permission -Identity $Identity -GroupedPermission $GroupedPermission -ErrorAction Stop
				} catch {
					Write-Log -Level Error -Message "Failed to add permission: $($_.Exception.Message)"
				}
			}
			"<=" {
				# Remove a permission
				try {
					Remove-Permission -Identity $Identity -GroupedPermission $GroupedPermission -ErrorAction Stop
				} catch {
					if ($_.CategoryInfo.Reason -eq 'InvalidExternalUserIdException') {
						# Exchange has an annoying bug where it can't remove permissions by email if
						# the AD account is disabled. Is this account disabled?
						$mailbox = Get-Mailbox -Identity $GroupedPermission.Group.Identity
						if ($mailbox.ExchangeUserAccountControl.ToString() -contains "AccountDisabled") {
							Remove-DisabledAccountPermission -UserMailbox $mailbox -ResourceIdentity $Identity
						} else {
							Write-Log -Level Error -Message "Failed to remove permission: $($_.Exception.Message)"
						}
					} else {
						Write-Log -Level Error -Message "Failed to remove permission: $($_.Exception.Message)"
					}
				}
			}
			default {
				# Something is wrong...
				Write-Log -Level Error -Message "Comparison indicator is not understood: $_"
			}
		}
	} elseif ($GroupedPermission.Count -eq 2) {
		Write-Log -Level Verbose -Message "Two permissions found in group"
		try {
			Set-Permission -Identity $Identity -GroupedPermission $GroupedPermission -ErrorAction Stop
		} catch {
			Write-Log -Level Error -Message "Failed to set permission: $($_.Exception.Message)"
		}
	} else {
		# What went wrong?
		Write-Log -Level Error -Message "Found more than 2 grouped permissions, something went wrong: $($GroupedPermission.Group)"
	}
}

function Compare-OldAndNewPermissions {
	[CmdletBinding()]
	param
	(
		[Parameter(Mandatory = $true)]
		$CurrentPermissions,
		
		[Parameter(Mandatory = $true)]
		$NewPermissions,
		
		[Parameter(Mandatory = $true)]
		[string[]]$Property,
		
		[Parameter(Mandatory = $true)]
		[System.Xml.XmlElement]$ResourceConfiguration
	)
	
	
	$compared = Compare-Object -ReferenceObject $currentPermissions -DifferenceObject $newPermissions -Property $Property
	
	if ((Get-BooleanValue -Value $ResourceConfiguration.RemoveUnknown) -and (-not $CommitChanges)) {
		$compared = $compared | Where { $_.Type -ne "Unknown" }
	}
	
	return $compared | Group-Object -Property Identity
}


$TotalResources = $config.Configuration.Folders.ChildNodes.Count
$counter = 1
:ResourceLoop foreach ($resource in $config.Configuration.Folders.ChildNodes) {
	$identity = $(Join-FolderPath -ResourceElement $resource)
	
	Write-Log -Level Info -Message "$("{0:d2}" -f $counter)/$("{0:d2}" -f $TotalResources): $identity" -ConsoleForeground Green
	
	if (-not (Get-BooleanValue -Value $resource.Enabled)) {
		Write-Log -Level Warn -Message "$identity is disabled in config! Skipping!"
		continue
	}
	
	if (Get-BooleanValue -Value $resource.RemoveUnknown) {
		Write-Log -Level Info -Message "Checking for unknown permissions on $identity"
		Remove-UnknownPermissions -Identity $identity
	}
	
	Write-Log -Level Info -Message "Obtaining current permissions for $identity"
	$currentPermissions = Get-CurrentPermissions -Identity $identity
	
	Write-Log -Level Info -Message "Building new permissions for $identity"
	$newPermissions = Build-NewPermissions -ResourceElement $resource -MasterPermissions $config.Configuration.MasterPermissions
	
	Write-Log -Level Info -Message "Comparing permissions for $identity"
	$groupedPermissions = Compare-OldAndNewPermissions -CurrentPermissions $currentPermissions -NewPermissions $newPermissions -Property "Identity", "AccessRights" -ResourceConfiguration $resource
	
	if ($groupedPermissions) {
		:PermissionsLoop foreach ($group in $groupedPermissions) {
			Update-FolderPermissions -GroupedPermission $group -Identity $identity
		}
	} else {
		Write-Log -Level Info -Message "No permissions to change for $identity"
	}
	
	$counter++
}






















