## Synopsis

Automatically updates Exchange Calendar/Resource Mailbox Calendar permissions

## Code Example

Once you've configured the XML file properly for your environment, run the script as follows:

```
.\Invoke-UpdateCalendarPermissions.ps1 -Path ".\Configuration.xml"
```

This will loop through the resources you've defined in the `Configuration.xml` and add/remove/update the permissions

The `Configuration.xml` file contains some examples to get you started. There are also some placeholders for things I haven't (maybe don't plan to?) implement.

## Motivation

My company's users likes to have direct access to the calendar. This works....okay, for a group of about 30. But when people come and go its difficult to update 10 or so resource mailbox calendars manually.

## Contributors

Feel free to clone and fork or open issues. I prefer to submit pull-requests to resolve those issues though if you can :-)

## License

MIT LICENSE 

Copyright 2016 Benjamin Claussen

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.